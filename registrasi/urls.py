from django.urls import path, include
from . import views

urlpatterns = [
    # path('', include('django.contrib.auth.urls')),
    path('signup/',views.pickrole, name='pickrole'),
    path('signup/admin/',views.adminsignup, name='adminsignup'),
    path('signup/petugas/',views.petugassignup, name='petugassignup'),
    path('post-signup/petugas/',views.postPetugasSignup, name="postpetugassignup"),
    path('signup/anggota/',views.anggotasignup, name='anggotasignup'),
    path('post-signup/anggota/',views.postAnggotaSignup, name="postanggotasignup"),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('post-login/', views.postLogin, name="postlogin"),
    
]
