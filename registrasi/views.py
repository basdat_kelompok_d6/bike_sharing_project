from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from random import randint



# Create your views here.

def login(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:

        return render(request,'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')

def postLogin(request):
    ktp_belum_fix = str(request.POST['ktp'])
    email = request.POST['email']
    ktp=''
    counter = 0
    for i in range(0,9):
        if(counter == 3):
            ktp+='-'
            ktp= ktp + ktp_belum_fix[i]
            counter = 0
        else:
            ktp= ktp + ktp_belum_fix[i]

        counter+=1

    cursor=connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute("SELECT * from person where ktp='"+ktp+"' and email='"+email+"'")
    hasil=cursor.fetchone();
    if (hasil):
        cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
        hasil2 = cursor.fetchone();
        if(hasil2):
            cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
            nama = cursor.fetchall()
            for row in nama:
                nama = row[0]
            request.session['nama'] = nama
            request.session['ktp'] = ktp
            request.session['email'] = email
            request.session['role'] = 'petugas'
            return HttpResponseRedirect('/penugasan/list')
        else:
            cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
            hasil3 = cursor.fetchone()
            if(hasil3):
                cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
                nama = cursor.fetchall()
                for row in nama:
                    nama = row[0]
                request.session['nama'] = nama
                request.session['ktp'] = ktp
                request.session['email'] = email
                request.session['role'] = 'anggota'
                return HttpResponseRedirect('/sepeda/daftarSepeda')
            else:
                messages.error(request, "email atau ktp salah")
                return HttpResponseRedirect('/registrasi/login')
    else:
        messages.error(request, "email atau ktp salah")
        return HttpResponseRedirect('/registrasi/login')
        

def pickrole(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'pickrole.html')



def adminsignup(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')   
    else:
        return render(request,'signup-adin.html')
    
def petugassignup(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'signup-petugas.html')

def anggotasignup(request):
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    else:
        return render(request,'signup-anggota.html')

def postPetugasSignup(request):
    ktp_belum_fix = str(request.POST['ktp'])
    if(len(ktp_belum_fix) != 9):
        messages.error(request, "Register Gagal: No Ktp berupa angka 9 digit")
        return HttpResponseRedirect('/registrasi/signup/petugas/')
    
    else:
        nama= request.POST['nama']
        email= request.POST['email']
        hp = request.POST['hp']
        tanggal =request.POST['tanggal']
        alamat = request.POST['alamat']
        gaji='30000'
        ktp=''
        counter = 0
        for i in range(0,9):
            if(counter == 3):
                ktp+='-'
                ktp= ktp + ktp_belum_fix[i]
                counter = 0
            else:
                ktp= ktp + ktp_belum_fix[i]

            counter+=1
    
        cursor=connection.cursor()
        cursor.execute('set search_path to bike_station')
        cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
        hasil=cursor.fetchone();
        print(hasil)
        if (hasil):
            if len(hasil)>0:
                messages.error(request, "Register Gagal: KTP "+ktp+" sudah pernah diregistrasi")
                return HttpResponseRedirect('/registrasi/signup/petugas/')

        cursor=connection.cursor()
        cursor.execute('set search_path to bike_station')
        cursor.execute("SELECT * from person where email='"+email+"'")
        hasil=cursor.fetchone();
        if (hasil):
            if len(hasil)>0:
                messages.error(request, "Register Gagal: Email "+email+" sudah pernah diregistrasi")
                return HttpResponseRedirect('/registrasi/signup/petugas/')

        

        values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+hp+"'"
        query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)

        values = "'"+ktp+"',"+ gaji
        query = "INSERT INTO petugas (ktp,gaji) values (" + values + ")"
        cursor.execute(query)

        # request.session['ktp']=ktp
        # request.session['nama']=nama
        # request.session['email']=email
        # request.session['role'] ='anggota'

        messages.success(request, "Register Berhasil: "+ nama)
        return HttpResponseRedirect('/registrasi/login/')
        # cursor.execute('SELECT * from acara')
        # hasil=cursor.fetchall()
        # print(hasil)
        # return


    
   
def postAnggotaSignup(request):
    ktp_belum_fix = str(request.POST['ktp'])
    if(len(ktp_belum_fix) != 9):
        messages.error(request, "Register Gagal: No Ktp berupa angka 9 digit")
        return HttpResponseRedirect('/registrasi/signup/petugas/')
    
    else:
        nama= request.POST['nama']
        email= request.POST['email']
        hp = request.POST['hp']
        tanggal =request.POST['tanggal']
        alamat = request.POST['alamat']
        points = '0'
        saldo = '0'
        ktp=''
        counter = 0
        for i in range(0,9):
            if(counter == 3):
                ktp+='-'
                ktp= ktp + ktp_belum_fix[i]
                counter = 0
            else:
                ktp= ktp + ktp_belum_fix[i]

            counter+=1
        no_kartu = generate_no_kartu()
    
        cursor=connection.cursor()
        cursor.execute('set search_path to bike_station')
        cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
        hasil=cursor.fetchone();
        print(hasil)
        if (hasil):
            if len(hasil)>0:
                messages.error(request, "Register Gagal: KTP "+ktp+" sudah pernah diregistrasi")
                return HttpResponseRedirect('/registrasi/signup/anggota/')

        cursor=connection.cursor()
        cursor.execute('set search_path to bike_station')
        cursor.execute("SELECT * from person where email='"+email+"'")
        hasil=cursor.fetchone();
        if (hasil):
            if len(hasil)>0:
                messages.error(request, "Register Gagal: Email "+email+" sudah pernah diregistrasi")
                return HttpResponseRedirect('/registrasi/signup/anggota/')

        

        values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+hp+"'"
        query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)

        values = "'"+no_kartu+"',"+ saldo + "," + points +"," + "'"+ktp+"'"
        query = "INSERT INTO anggota (no_kartu,saldo,points,ktp) values (" + values + ")"
        cursor.execute(query)

        # request.session['ktp']=ktp
        # request.session['nama']=nama
        # request.session['email']=email
        # request.session['role'] ='anggota'

        messages.success(request, "Register Berhasil: "+ nama)
        return HttpResponseRedirect('/registrasi/login/')
        # cursor.execute('SELECT * from acara')
        # hasil=cursor.fetchall()
        # print(hasil)
        # return
    


def generate_no_kartu():
    kartu = randint(10000000, 99999999)
    kartu = str(kartu)
    no_kartu=''
    counter = 0
    for i in range(0,8):
        if(counter == 3):
            no_kartu+='-'
            no_kartu= no_kartu + kartu[i]
            counter = 0
        else:
            no_kartu= no_kartu + kartu[i]

        counter+=1

    cursor=connection.cursor()
    cursor.execute('set search_path to bike_station')
    cursor.execute("SELECT * from anggota where no_kartu='"+no_kartu+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            generate_no_kartu()
    
    else:
        return no_kartu
