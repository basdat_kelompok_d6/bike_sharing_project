from django.urls import path
from . import views
urlpatterns = [
    path('createSepeda/', views.createSepeda, name='createSepeda'),
    path('daftarSepeda/', views.daftarSepeda, name='daftarSepeda'),
    path('post-sepeda/',views.postSepeda, name = 'postSepeda'),
    path('delete-sepeda/',views.deleteSepeda, name = 'deleteSepeda'),
    path('update-sepeda/',views.updateSepeda, name = 'updateSepeda'),
    path('detail-update-sepeda/',views.detailUpdateSepeda, name = 'detailUpdateSepeda'),
    # path('pinjam-sepeda/',views.pinjamSepeda, name='pinjamSepeda'),
]
