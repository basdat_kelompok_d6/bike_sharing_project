from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def createSepeda(request):
  if(request.session['role']=='petugas'):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute("select p.nama from person as p, anggota as a where a.ktp=p.ktp")
    hasil1 = cursor.fetchall()
    response['penyumbang'] = hasil1
    print(response)
    cursor.execute("select nama from stasiun")
    hasil2 = cursor.fetchall()
    response['stasiun'] = hasil2
    print(response)

    
    return render(request, 'formsSepeda.html', response)
  else:
    return HttpResponseRedirect('/')

def daftarSepeda(request):
  response={}
  cursor = connection.cursor()
  cursor.execute('set search_path to bike_station,public')
  cursor.execute("select s.nama, p.nama, n.nomor, n.merk, n.jenis, n.status from sepeda as n, stasiun as s, person as p, anggota as a where n.no_kartu_penyumbang = a.no_kartu and a.ktp = p.ktp and n.id_stasiun = s.id_stasiun; ")
  hasil1 = cursor.fetchall()
  response['sepeda'] = hasil1
  print(response)
  return render(request, 'daftarSepeda.html',response)


def postSepeda(request):
  if(request.method=="POST"):
    merk = request.POST['merk']
    jenis = request.POST['jenis']
    status = request.POST['status']
    stasiun = request.POST['stasiun']
    penyumbang = request.POST['penyumbang']
    print(status)
    statusNow = False
    if(str(status)=='Tersedia'):
      statusNow = True
    else:
      statusNow = False
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+penyumbang+"' and p.ktp = a.ktp;") 
    hasil = cursor.fetchone()
    no_kartu_penyumbang = hasil[0]
    # cursor.execute("select id_stasiun from stasiun where nama='"+stasiun+"' ")
    cursor.execute("select id_stasiun from stasiun where nama='"+stasiun+"';")
    hasil2 = cursor.fetchone()
    id_stasiun = hasil2[0]
  
    # id_stasiun = cursor.fetchall()
    # for row in id_stasiun:
    #   id_stasiun = row[0]
    # print(id_stasiun)

    # cursor.execute("select nomor from sepeda")
    cursor.execute("select nomor from sepeda order by cast (nomor as int) desc;")
    hasil1 = cursor.fetchone()
    nomor = int(hasil1[0]) +1

    cursor.execute("INSERT INTO sepeda(nomor,merk,jenis,status,id_stasiun,no_kartu_penyumbang) values("+"'"+str(nomor)+"','" +str(merk)+"','" + str(jenis) +"','" + str(statusNow) +"', '" + str(id_stasiun) +"', '" + str(no_kartu_penyumbang) +"' );")
    messages.error(request, "success")
    return HttpResponseRedirect('/sepeda/daftarSepeda')
  else:
    messages.error(request, "gagal")
    return HttpResponseRedirect('/sepeda/createSepeda')

@csrf_exempt
def deleteSepeda(request):
    if(request.method=="POST"):
        nomor= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("delete from sepeda where nomor='"+nomor+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/sepeda/daftarSepeda')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/daftarSepeda')


@csrf_exempt
def updateSepeda(request):
    if(request.method=="POST"):
        merk = request.POST['merk']    
        jenis = request.POST['jenis']
        status = request.POST['status']
        stasiun = request.POST['stasiun']
        penyumbang = request.POST['penyumbang']
        nomor = request.POST['nomor']

        statusNow = False
        if(str(status)=='Tersedia'):
          statusNow = True
        else:
          statusNow = False

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')

        cursor.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+penyumbang+"' and p.ktp = a.ktp")
        hasil = cursor.fetchone()
        no_kartu = hasil[0]

        cursor.execute("update sepeda set merk='"+merk+"', jenis='"+jenis+"', status='"+str(statusNow)+"', no_kartu_penyumbang='"+no_kartu+"' where nomor = '"+str(nomor)+"';")
    
        # cursor.execute("select s.nama from stasiun as s, stasiun_stasiun as a where a.id_stasiun ='"+id_stasiun+"' and a.id_stasiun = s.id_stasiun;")
        # nama_stasiun = cursor.fetchone()
        # nama_stasiun = nama_stasiun[0]

        # cursor.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+nama+"' and p.ktp = a.ktp;") 
        # hasil = cursor.fetchone()
        # no_kartu_penyumbang = hasil[0]

        # cursor.execute("select id_stasiun from stasiun  where nama ='"+nama+"' ")
        # id_stasiun_lama = cursor.fetchone()
        # id_stasiun_lama=id_stasiun_lama[0]

        # cursor.execute("select id_stasiun from stasiun  where nama ='"+stasiun+"' ")
        # id_stasiun = cursor.fetchall()
        
        # for row in id_stasiun:
        #     id_stasiun = row[0]
            

        
        #cursor.execute("update acara_stasiun set id_stasiun='"+id_stasiun+"' where id_acara='"+id_acara+"' and id_stasiun='"+id_stasiun_lama+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/sepeda/daftarSepeda')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/daftarSepeda')

@csrf_exempt
def detailUpdateSepeda(request):
    if(request.method=="POST"):
        response={}
        sepeda= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select * from sepeda where nomor='"+sepeda+"';")
        stasiun = cursor.fetchone()
        # cursor.execute("select nama from stasiun")  
        # stasiun = cursor.fetchall()

        cursor.execute("select s.nama from stasiun as s, sepeda as a where a.nomor ='"+sepeda+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchone()
        #cursor.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+nama+"' and p.ktp = a.ktp;") 
        cursor.execute("select p.nama from person as p, anggota as a, sepeda as n where n.nomor='"+sepeda+"' and a.no_kartu=n.no_kartu_penyumbang and p.ktp=a.ktp;")
        hasil = cursor.fetchone()
        nama_penyumbang = hasil[0]

        cursor.execute("select jenis from sepeda where nomor='"+sepeda+"';")
        jenis = cursor.fetchone()

        cursor.execute("select status from sepeda where nomor='"+sepeda+"';")
        status = cursor.fetchone()

        cursor.execute("select merk from sepeda where nomor='"+sepeda+"';")
        merk = cursor.fetchone()

        cursor.execute("select p.nama from person as p, anggota as a where a.ktp=p.ktp")
        penyumbang = cursor.fetchall()

        cursor.execute("select nama from stasiun")
        stasiun = cursor.fetchall()
        response['stasiun'] = stasiun
        response['penyumbang'] = penyumbang
        response['sepeda'] = sepeda
        response['nama_stasiun'] = nama_stasiun
        response['nama_penyumbang'] = nama_penyumbang
        response['jenis'] = jenis[0]
        response['merk'] = merk[0]
        response['status'] = status[0]

        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/daftarSepeda')

# @csrf_exempt
# def pinjamSepeda(request):
#   if(request.method=='POST'):
#     nomor_sepeda = request.POST['nomor_sepeda']
#     ktp=request.session['ktp']

#     cursor = connection.cursor()
#     cursor.execute("set search_path to bike_station, public")
#     cursor.execute("update sepeda set status='False' where nomor='"+nomor_sepeda+"';")

#     cursor.execute("select no_kartu from anggota where ktp='"+ktp+"';")
#     hasil = cursor.fetchone()
#     no_kartu_anggota = hasil[0]

#     cursor.execute("INSERT INTO peminjaman(no_kartu_anggota,datetime_pinjam,nomor_sepeda,id_stasiun,datetime_kembali,biaya,denda")

#   else:
#     messages.error(request, "gagal")
#     return HttpResponseRedirect('/sepeda/daftarSepeda')