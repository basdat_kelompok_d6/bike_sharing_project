from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def createStasiun(request):
    if(request.session['role']=='petugas'):
        response={}
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select nama from stasiun")
        
        hasil1 = cursor.fetchall()
        response['stasiun'] = hasil1
        print(response)
        return render(request, 'formsStasiun.html',response)
    else:
        return HttpResponseRedirect('/')

def daftarStasiun(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute("select * from stasiun")
    hasil1 = cursor.fetchall()
    response['stasiun'] = hasil1
    print(response)
    return render(request, 'daftarStasiun.html',response)

def postStasiun(request):
    if(request.method=="POST"):
        nama = request.POST['nama']    
        alamat = request.POST['alamat']
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
        
      
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select id_stasiun from stasiun")
        id_stasiun = cursor.fetchall()
        for row in id_stasiun:
            id_stasiun = row[0]
        
        cursor.execute("select id_stasiun from stasiun order by cast (id_stasiun as int) desc;")
        hasil = cursor.fetchone()
        id_stasiun = int(hasil[0]) +1
   

        if longitude == '' and latitude !='':
             cursor.execute("INSERT INTO stasiun(id_stasiun,alamat,lat,nama) values("+"'"+str(id_stasiun)+"','" +str(alamat)+"'," + str(latitude) +",'"  + str(nama)+"');")
        elif latitude == '' and longitude !='':
             cursor.execute("INSERT INTO stasiun(id_stasiun,alamat,long,nama) values("+"'"+str(id_stasiun)+"','" +str(alamat)+"','" +  str(longitude) +"','" + str(nama)+"');")
        elif longitude =='' and latitude == '':
             cursor.execute("INSERT INTO stasiun(id_stasiun,alamat,nama) values("+"'"+str(id_stasiun)+"','" +str(alamat)+"','" +  str(nama)+"');")
        else:
            cursor.execute("INSERT INTO stasiun(id_stasiun,alamat,lat,long,nama) values("+"'"+str(id_stasiun)+"','" +str(alamat)+"'," + str(latitude) +",'" + str(longitude) +"','" + str(nama)+"');")
        messages.error(request, "success")
        return HttpResponseRedirect('/stasiun/daftarStasiun')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/createStasiun')

@csrf_exempt
def deleteStasiun(request):
    if(request.method=="POST"):
       
        id_stasiun= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("delete from stasiun where id_stasiun='"+id_stasiun+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/stasiun/daftarStasiun')

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/daftarStasiun')

@csrf_exempt
def updateStasiun(request):
    if(request.method=="POST"):
        print("aaaaaaahfakhkshfkahfkah")
        nama = request.POST['nama']    
        alamat = request.POST['alamat']
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
        id_stasiun= request.POST['id_stasiun']

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("update stasiun set nama='"+nama+"', alamat='"+alamat+"', lat='"+latitude+"', long='"+longitude+"' where id_stasiun = '"+id_stasiun+"';")
      
        # cursor.execute("select s.nama from stasiun as s, stasiun_stasiun as a where a.id_stasiun ='"+id_stasiun+"' and a.id_stasiun = s.id_stasiun;")
        # nama_stasiun = cursor.fetchone()
        # nama_stasiun = nama_stasiun[0]

        # cursor.execute("select id_stasiun from stasiun  where nama ='"+nama+"' ")
        # id_stasiun_lama = cursor.fetchone()
        # id_stasiun_lama=id_stasiun_lama[0]

        # cursor.execute("select id_stasiun from stasiun  where nama ='"+stasiun+"' ")
        # id_stasiun = cursor.fetchall()
        
        # for row in id_stasiun:
        #     id_stasiun = row[0]
            

        
        #cursor.execute("update acara_stasiun set id_stasiun='"+id_stasiun+"' where id_acara='"+id_acara+"' and id_stasiun='"+id_stasiun_lama+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/stasiun/daftarStasiun')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/daftarStasiun')

@csrf_exempt
def detailUpdateStasiun(request):
    if(request.method=="POST"):
        response={}

       
        stasiun= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select * from stasiun where id_stasiun='"+stasiun+"';")
        stasiun = cursor.fetchone()
        # cursor.execute("select nama from stasiun")  
        # stasiun = cursor.fetchall()
        # cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='"+acara+"' and a.id_stasiun = s.id_stasiun;")
        # nama_stasiun = cursor.fetchone()
        response['stasiun'] = stasiun
        # response['acara'] = id_acara
        # response['nama_stasiun'] = nama_stasiun[0]
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/daftarStasiun')


