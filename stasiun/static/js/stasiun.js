function deleteList(x){
    $(".cd-popup").addClass("is-visible")
    $(".cd-popup").attr('id',x)
}

function updateStasiun(){
    id_stasiun = $(".modal-body").attr('id')
    nama = $('#namaStasiun').val()
    if(nama==''){
        nama = $('#namaStasiun').attr('placeholder')
    }
    alamat = $('#alamatStasiun').val()
    if(alamat==''){
        alamat=$('#alamatStasiun').attr('placeholder')
    }
    latitude = $('#Latitude').val()
    if(latitude==''){
        latitude = $('#Latitude').attr('placeholder')
    }
    longitude = $('#Longitude').val()
    if(longitude==''){
        longitude = $('#Longitude').attr('placeholder')
    }
    // stasiun = $('#inputStasiun').val()

    $.ajax(
        {
          url: "/stasiun/update-stasiun/",
            datatype: 'json',
            data: {
                'id_stasiun':id_stasiun,
                'alamat':alamat,
                'latitude':latitude,
                'longitude':longitude,
                'nama':nama,
            },
          method:'POST',
          success: function(result){
            location.reload()
            } ,
            error:function(a){
                alert(a)
                location.reload();

            }
           
        }
    );

}

function cancelFunction(){
    $('.cd-popup').removeClass('is-visible')
}

function yesFunction(){
    id = $(".cd-popup").attr('id')
    alert(id)
    $.ajax(
        {
          url: "/stasiun/delete-stasiun/",
            datatype: 'json',
            data: {'id':id},
          method:'POST',
          success: function(result){
            location.reload()
            }  
        }
        );
}
function updateModal(x){
    alert(x)
    $(".modal-body").attr('id',x) 
    $(".spinner-border").addClass("true")
    $(".spinner-border").removeClass("false")
    $("#load").addClass("false")
    $("#load").removeClass("true")
    $.ajax(
        {
          url: "/stasiun/detail-update-stasiun/",
            datatype: 'json',
            data: {'id':x},
          method:'POST',
          success: function(result){
            $(".spinner-border").addClass("false")
            $(".spinner-border").removeClass("true")
            $("#load").addClass("true")
            $("#load").removeClass("false")
            console.log(result)
            stasiun=''
            for(i=0; i<result.stasiun.length;i++){
                stasiun+='<option>'+result.stasiun[i]+'</option>'
            }
            $('#namaStasiun').attr('placeholder',result.stasiun[4])
            $('#alamatStasiun').attr('placeholder',result.stasiun[1])
            $('#Latitude').attr('placeholder',result.stasiun[3])
            $('#Longitude').attr('placeholder',result.stasiun[2])
           
            // $('#inputStasiun').html(
            //     `<option selected>`+result.nama_stasiun+`</option>`+
            //     stasiun         
            // )
            }  
        }
        );

}
        
      