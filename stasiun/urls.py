# from django.urls import path, include
# from .views import *
# app_name = 'stasiun'
# urlpatterns = [
#     path('', stasiun),
#     path('', daftarStasiun)
# ]
from django.urls import path
from . import views

urlpatterns = [
    path('createStasiun/', views.createStasiun, name='createStasiun'),
    path('daftarStasiun/', views.daftarStasiun, name='daftarStasiun'),
    path('post-stasiun/',views.postStasiun, name = 'postStasiun'),
    path('delete-stasiun/',views.deleteStasiun, name = 'deleteStasiun'),
    path('update-stasiun/',views.updateStasiun, name = 'updateStasiun'),
    path('detail-update-stasiun/',views.detailUpdateStasiun, name = 'detailUpdateStasiun'),
]