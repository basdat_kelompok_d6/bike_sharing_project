from django.urls import path
from . import views

urlpatterns = [
    path('create/',views.create, name = 'create'),
    path('list/',views.list, name = 'list'),
    path('delete-penugasan/',views.delete_penugasan, name = 'deletePenugasan'),
    path('post-penugasan/',views.post_penugasan, name = 'postPenugasan'),
    path('delete-penugasan/',views.delete_penugasan, name = 'deletePenugasan'),
    path('update-penugasan/',views.update_penugasan, name = 'updatePenugasan'),
    path('detail-penugasan/',views.detailPenugasan, name='detailPenugasan'),
    
]
