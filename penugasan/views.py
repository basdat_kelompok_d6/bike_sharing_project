from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def create(request):
    if(request.session['role']=='petugas'):
        response={}
        cursor = connection.cursor()

        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select nama from stasiun")
        hasil1 = cursor.fetchall()
        response['stasiun'] = hasil1

        cursor.execute("select penugasan.ktp, person.nama from penugasan, person where person.ktp = penugasan.ktp;")
        petugas = cursor.fetchall()
        response['petugas'] = petugas

        return render(request, 'forms.html',response)
    else:
        return HttpResponseRedirect('/')

def list(request):
    if(request.session['role']=='petugas'):
        response={}
        cursor = connection.cursor()

        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select p.ktp, per.nama, p.start_datetime, p.end_datetime, s.nama from stasiun as s, penugasan as p, person as per where s.id_stasiun = p.id_stasiun and p.ktp = per.ktp;")
        hasil1 = cursor.fetchall()
        response['penugasan'] = hasil1

        return render(request, 'daftarPenugasan.html',response)
    else:
        return HttpResponseRedirect('/')


def post_penugasan(request):
    if(request.method == 'POST'):
        petugas = request.POST['petugas']
        mulai = request.POST['mulai']
        selesai = request.POST['selesai']
        stasiun = request.POST['stasiun']
        a = petugas.split('-')
        ktp = "-".join(a[:3])
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select id_stasiun from stasiun where nama ='"+stasiun+"';")
        id_stasiun = cursor.fetchone()
        cursor.execute("INSERT INTO penugasan(ktp,start_datetime,id_stasiun,end_datetime) values("+"'"+str(ktp)+"','" +mulai+"','" +str(id_stasiun[0]) +"','" + selesai + "');")
        
        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan/list')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/create')
    
@csrf_exempt
def delete_penugasan(request):
    if(request.method=="POST"):
        petugas= request.POST['ktp']
        a = petugas.split('-')
        no_ktp = "-".join(a[:3])
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("delete from penugasan where ktp='"+no_ktp+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan/list')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/create')

@csrf_exempt
def update_penugasan(request):
    if(request.method == "POST"):
        petugas = request.POST['petugas']
        a = petugas.split('-')
        no_ktp_baru = "-".join(a[:3])
        mulai = request.POST['mulai']    
        selesai = request.POST['selesai']
        stasiun= request.POST['stasiun']
        ktp_lama=request.POST['ktp']

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        
        # cursor.execute("select ktp from penugasan")
        # petugas_lama = cursor.fetchone()
        # petugas_lama = petugas_lama[0]

        cursor.execute("select start_datetime from penugasan where ktp='"+ktp_lama+"';")
        date_lama = cursor.fetchone()
        date_lama = date_lama[0]

        cursor.execute("select id_stasiun from penugasan where ktp ='"+ktp_lama+"';")
        stasiun_lama = cursor.fetchone()
        stasiun_lama = stasiun_lama[0]

        cursor.execute("select id_stasiun from stasiun where nama='"+stasiun+"';")
        id_stasiun_baru=cursor.fetchone()
        id_stasiun_baru=id_stasiun_baru[0]
        cursor.execute("update penugasan set ktp = '"+no_ktp_baru+"',start_datetime = '"+str(mulai)+"', id_stasiun = '"+id_stasiun_baru+"', end_datetime = '"+str(selesai)+"' where ktp ='"+ktp_lama+"'and start_datetime = '"+str(date_lama)+"'and id_stasiun = '"+stasiun_lama+"';")
        
        messages.error(request, "success")
        return HttpResponseRedirect('/penugasan/daftarPenugasan')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/daftarPenugasan')

@csrf_exempt
def detailPenugasan(request):
    if(request.method=="POST"):
        response={}

       
        ktp= request.POST['ktp']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select * from penugasan where ktp='"+ktp+"';")
        penugasan = cursor.fetchall()
        cursor.execute("select nama from person where ktp='"+ktp+"';")  
        nama = cursor.fetchone()
        nama_stasiun = cursor.fetchall()
        cursor.execute("select s.nama from stasiun as s, penugasan as p where p.ktp='"+ktp+"' and p.id_stasiun=s.id_stasiun")
        stasiun=cursor.fetchone()
        cursor.execute("select nama from stasiun")
        list_stasiun = cursor.fetchall()
        cursor.execute("select penugasan.ktp, person.nama from penugasan, person where person.ktp = penugasan.ktp;")
        petugas = cursor.fetchall()
        response['petugas'] = petugas
        response['penugasan'] = penugasan[0]
        response['nama'] = nama[0]
        response['stasiun'] = stasiun[0]
        response['list_stasiun'] = list_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/penugasan/list/')