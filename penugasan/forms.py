from django.db import models
from django import forms


class Create_Form(forms.Form):
    attrs = {
        'type' : 'text',
        'class' : 'todo-form-input',
    }
    petugas = forms.CharField(label = 'Petugas', required = True, max_length = 20,widget = forms.TextInput(attrs=attrs))
    tanggal_mulai = forms.DateField(label = 'Tanggal Mulai',required=True,widget=forms.DateInput(attrs={'type':'date'}))
    tanggal_selesai = forms.DateField(label = 'Tanggal Selesai',required=True,widget=forms.DateInput(attrs={'type':'date'}))
    stasiun = forms.CharField(label = 'Stasiun',max_length = 50, required = True,widget=forms.TextInput(attrs=attrs))