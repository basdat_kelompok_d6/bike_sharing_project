from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt



# Create your views here.

# def create(request):
#     return render(request, 'create.html')

# def lists(request):
#     return render(request, 'lists.html')


# Create your views here.
def create(request):
    if(request.session['role']=='petugas'):
        response = {}
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select kategori from voucher")

        hasil1 = cursor.fetchall()
        response['voucher'] = hasil1
        print(response)
        return render(request, 'create.html', response)
    else:
        return HttpResponseRedirect('/')


def lists(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute("select * from voucher")
    hasil1 = cursor.fetchall()
    response['voucher'] = hasil1
    print(response)

    return render(request, 'lists.html', response)


def postVoucher(request):
    if (request.method == "POST"):
        nama = request.POST['nama']
        kategori= request.POST['kategori']
        poin = request.POST['nilai_poin']
        deskripsi = request.POST['deskripsi']
        jumlah = request.POST['jumlah']
        ktp_petugas = request.session['ktp']
        print(ktp_petugas)
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select id_voucher from voucher order by cast (id_voucher as int) desc;")
        hasil = cursor.fetchone()
        id_voucher = int(hasil[0]) + 1
        # cursor.execute("select no_kartu from ktp where ktp = '" + str(ktp_petugas) + "' ;")
        # no_anggota = cursor.fetchone()
        # # no_anggota = no_anggota[1]
        # print(no_anggota)

        print(id_voucher)
        cursor.execute("INSERT INTO voucher(id_voucher, nama, kategori, nilai_poin, deskripsi, jumlah) values(" + "'" + str(
            id_voucher) + "','" + str(nama) + "','" + kategori + "'," + poin + ",'" + deskripsi + "','" + jumlah
            + "');")
        messages.error(request, "success")
        return HttpResponseRedirect('/voucher/lists')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/voucher/create')


@csrf_exempt
def deleteVoucher(request):
    if (request.method == "POST"):

        id_voucher = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("delete from voucher where id_voucher='" + id_voucher + "';")
        messages.error(request, "success")
        return HttpResponseRedirect('/voucher/lists')

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/voucher/lists')


@csrf_exempt
def updateVoucher(request):
    if (request.method == "POST"):
        id_voucher = request.POST['id_voucher']
        nama = request.POST['nama']
        kategori = request.POST['kategori']
        poin = request.POST['poin']
        deskripsi = request.POST['deskripsi']
        jumlah = request.POST['jumlah']
        print(jumlah)
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("update voucher set nama='" + nama + "', kategori='" +
            kategori + "', nilai_poin='" + poin + "', deskripsi='" + deskripsi + "', jumlah='" + jumlah + "' where id_voucher = '" + id_voucher + "';")
        # cursor.execute(
        #     "select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" + id_acara + "' and a.id_stasiun = s.id_stasiun;")
        # nama_stasiun = cursor.fetchall()
        # for i in range(0, len(list_stasiun)):
        #
        #     id_stasiun_lama = ''
        #     id_stasiun = ''
        #     for i in nama_stasiun:
        #         print(i[0])
        #         cursor.execute("select id_stasiun from stasiun  where nama ='" + i[0] + "' ")
        #         id_stasiun_lama = cursor.fetchone()
        #         id_stasiun_lama = id_stasiun_lama[0]
        #
        #     for j in list_stasiun:
        #         cursor.execute("select id_stasiun from stasiun  where nama ='" + j + "' ")
        #         id_stasiun = cursor.fetchone()
        #         id_stasiun = id_stasiun[0]
        #
        #     cursor.execute(
        #         "update acara_stasiun set id_stasiun='" + id_stasiun + "' where id_acara='" + id_acara + "' and id_stasiun='" + id_stasiun_lama + "';")

        messages.error(request, "success")
        return HttpResponseRedirect('/voucher/lists')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/voucher/lists')


@csrf_exempt
def detailUpdateVoucher(request):
    if (request.method == "POST"):
        response = {}

        voucher = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select * from voucher where id_voucher='" + voucher + "';")
        id_voucher = cursor.fetchone()
        cursor.execute("select kategori from voucher")
        kategori = cursor.fetchall()
        # cursor.execute(
        #     "select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" + acara + "' and a.id_stasiun = s.id_stasiun;")
        # nama_stasiun = cursor.fetchall()
        response['kategori'] = kategori
        response['voucher'] = id_voucher
        # response['nama_stasiun'] = nama_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/voucher/lists')
