from django.urls import path
from . import views

urlpatterns = [
    path('create/', views.create, name='create'),
    path('lists/', views.lists, name='lists'),
    path('post-voucher/',views.postVoucher, name = 'postVoucher'),
    path('delete-voucher/',views.deleteVoucher, name = 'deleteVoucher'),
    path('update-voucher/',views.updateVoucher, name = 'updateVoucher'),
    path('detail-update-voucher/',views.detailUpdateVoucher, name = 'detailUpdateVoucher'),
]
