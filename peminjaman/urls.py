from django.urls import path, include
from . import views

urlpatterns = [
    path('create/', views.createPeminjaman, name='createPeminjaman'),   
    path('daftar-peminjaman/',views.daftarPeminjaman, name='daftarPeminjaman'),
    path('post-peminjaman/',views.postPeminjaman, name = 'postPeminjaman'),
    # path('delete-peminjaman/',views.deleteVoucher, name = 'deleteVoucher'),
    # path('update-peminjaman/',views.updateVoucher, name = 'updateVoucher'),
    # path('detail-update-peminjaman/',views.detailUpdateVoucher, name = 'detailUpdateVoucher'),
]
