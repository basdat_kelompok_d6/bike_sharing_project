from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def daftarPeminjaman (request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute('select p.no_kartu_anggota, p.datetime_pinjam, sp.merk, st.nama, p.datetime_kembali, p.biaya, p.denda from peminjaman as p, sepeda as sp, stasiun as st where p.nomor_sepeda = sp.nomor and p.id_stasiun = st.id_stasiun;')
    peminjaman = cursor.fetchall()
    response['peminjaman'] = peminjaman
    return render (request, 'daftarPeminjaman.html', response)

def createPeminjaman (request):
    # return render (request, 'createPeminjaman.html')
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_station,public')
    cursor.execute("select merk, nama from sepeda as sp, stasiun as st where sp.id_stasiun = st.id_stasiun")
    sepeda_stasiun = cursor.fetchall()
    response['sepeda_stasiun'] = sepeda_stasiun
    print(response)
    return render(request, 'createPeminjaman.html', response)

def postPeminjaman(request):
    if (request.method == "POST"):
        sepeda_stasiun = request.POST['sepeda_stasiun']
        sepeda_stasiun2 = sepeda_stasiun.split("-")
        merk_sepeda = sepeda_stasiun2[0]
        nama_stasiun = sepeda_stasiun2[1]
        no_ktp = request.session['ktp']
        print(nama_stasiun)
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select no_kartu from anggota where ktp = '" + str(no_ktp)+"';")
        no_kartu_anggota = cursor.fetchone()
        no_kartu_anggota = no_kartu_anggota[0]
        cursor.execute("select nomor from sepeda where merk = '" + merk_sepeda + "';")
        nomor_sepeda = cursor.fetchone()
        nomor_sepeda = nomor_sepeda[0]
        print(nomor_sepeda)
        cursor.execute("select id_stasiun from stasiun where nama = '" + nama_stasiun + "';")
        id_stasiun = cursor.fetchone()
        id_stasiun = id_stasiun[0]
        cursor.execute("set timezone = 'asia/jakarta'")
        cursor.execute("INSERT INTO peminjaman(no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun) values(" + "'" + no_kartu_anggota + "', now()::timestamp, " +
                        "'" + nomor_sepeda + "', " + "'" + id_stasiun + "');")
        messages.error(request, "success")
        return HttpResponseRedirect('/peminjaman/daftar-peminjaman')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/peminjaman/create')
