# BIKE-SHARING 

Basis Data Final Projects
Kelompok 6 
Kelas D

## Meet Our Team

* **Dinda Mutiara Qur'ani Putri** - *1706984556* - bagian 5-8
* **Siti Nurfitriyah** - *1706984745* - bagian 9-12
* **Muhamad Istiady Kartadibrata** - *1706025283* - bagian 1-4
* **Naufaldi Athallah Rifqi** - *1706023782* -bagian 13-16


### Prerequisites

1. Clone Repo ini

```
git clone https://gitlab.com/basdat_kelompok_d6/bike_sharing_project.git
```

2. Buat branch baru

```
git checkout -b 'nama branch'
```

3. Bikin app baru di branch itu

```
python3 manage.py startapp 'nama-app'
```

4. push nya ke branch masing-masing, kalo udah selesai semua tinggal merge request ke master

