from django.urls import path
from . import views

urlpatterns = [
    path('riwayat/', views.riwayatTransaksi, name='riwayatTransaksi'),
    path('topup/', views.topup, name='topup'),
     path('laporan/', views.laporan, name='laporan'),
     path('post-topup/',views.postTopup, name="post-topup")

]
