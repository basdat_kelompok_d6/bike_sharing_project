from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages


# Create your views here.
def riwayatTransaksi(request):
    if(request.session['role']=='anggota'):
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        response={}
        ktp = request.session['ktp']
        cursor.execute("select t.date_time,t.jenis,t.nominal from anggota as a, transaksi as t where a.ktp='"+ktp+"' and t.no_kartu_anggota = a.no_kartu;")
        
        hasil1 = cursor.fetchall()
        response['transaksi'] = hasil1
        print(response)

        return render (request, 'transaksi.html',response)
    else:
        return HttpResponseRedirect('/')

def topup(request):
    if(request.session['role']=='anggota'):
    
        return render (request, 'topup.html')
    else:
        return HttpResponseRedirect('/')

def laporan(request):
    if(request.session['role']=='petugas'):
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        response={}
        cursor.execute("select n.nama, l.id_laporan,l.no_kartu_anggota, l.datetime_pinjam, l.status, p.denda from person as n, laporan as l,  anggota as a, peminjaman as p where  n.ktp = a.ktp and a.no_kartu = l.no_kartu_anggota and l.no_kartu_anggota = p.no_kartu_anggota and l.datetime_pinjam= p.datetime_pinjam and l.nomor_sepeda = p.nomor_sepeda;")
        hasil1 = cursor.fetchall()
        response['laporan'] = hasil1
        print(response)
        return render (request, 'laporan.html', response)
    else:
        return HttpResponseRedirect('/')

def postTopup(request):  
    
    if(request.method=="POST"):
        inputSaldo = request.POST['topup']    
        ktp = request.session['ktp']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        # cursor.execute("select saldo from anggota  where ktp ='"+ktp+"' ")
        # saldo = cursor.fetchall()
        # print(inputSaldo)
        # for row in saldo:
        #     saldo = row[0]
        
        # saldoakhir = int(saldo) + int(inputSaldo)
        # cursor.execute("update anggota set saldo = '"+str(saldoakhir)+"' where ktp ='111-111-111';")
        cursor.execute("select no_kartu from anggota where ktp='"+ktp+"';") 
        kartu=cursor.fetchone()
        kartu=kartu[0]
        cursor.execute("set timezone='asia/jakarta';")
        cursor.execute("insert into transaksi(no_kartu_anggota,date_time,jenis,nominal)values('"+str(kartu)+"',now()::timestamp,'TopUp','"+inputSaldo+"')")
        messages.error(request, "success")
        return HttpResponseRedirect('/transaksi/riwayat')
        
    else:
        messages.error(request, "transaksi gagal")
        return HttpResponseRedirect('/transaksi/topup')
