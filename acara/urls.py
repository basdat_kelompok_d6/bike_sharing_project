from django.urls import path
from . import views

urlpatterns = [
    path('createAcara/',views.createAcara, name = 'createAcara'),
    path('daftarAcara/',views.daftarAcara, name = 'daftarAcara'),
    path('post-acara/',views.postAcara, name = 'postAcara'),
    path('delete-acara/',views.deleteAcara, name = 'deleteAcara'),
    path('update-acara/',views.updateAcara, name = 'updateAcara'),
    path('detail-update-acara/',views.detailUpdateAcara, name = 'detailUpdateAcara'),
]
