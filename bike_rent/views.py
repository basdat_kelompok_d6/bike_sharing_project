from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json



# Create your views here.

# Create your views here.
def index (request):
   return render (request, 'home.html')


@csrf_exempt
def saldo(request):
   response ={}
   if(request.method=="POST"):
      cursor = connection.cursor()
      cursor.execute('set search_path to bike_station,public')
      ktp = request.session['ktp']
      
      cursor.execute("select saldo from anggota  where ktp ='"+ktp+"' ")
      saldo = cursor.fetchall()
      for row in saldo:
         saldo = row[0]
      response["saldoNow"] = saldo
      return JsonResponse(response)

   else:
      return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")



# try:
# 		q = request.GET['q']
# 	except:
# 		q = 'quilting'

# 	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ q)
# 	jsonparse = json.dumps(getJson.json())
# 	return HttpResponse(jsonparse)
